<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateScoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('scores', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('group_user_id')->unsigned();
            $table->foreign('group_user_id')->references('id')->on('group_users')->onDelete('cascade');
            $table->integer('match_id')->unsigned();
            $table->integer('points');
            $table->foreign('match_id')->references('id')->on('matches')->onDelete('cascade');
            $table->enum('status', ['accepted', 'declined', 'pending']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('scores');
    }
}
