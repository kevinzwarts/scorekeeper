<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        DB::table('users')->delete();

        $users = array(
            ['name' => 'Kevin Zwarts', 'username' => 'kevinwardog', 'email' => 'kevin@zwarts.nl', 'password' => Hash::make('secret')],
            ['name' => 'Mike Lauppe', 'username' => 'mikeshadow', 'email' => 'mike@lauppe.nl', 'password' => Hash::make('secret')],
            ['name' => 'Iris Mensink', 'username' => 'irisgirl', 'email' => 'iris@mensink.nl', 'password' => Hash::make('secret')],
            ['name' => 'Tessa Mensink', 'username' => 'tessaaa', 'email' => 'tessa@mensink.nl', 'password' => Hash::make('secret')],
        );

        // Loop through each user above and create the record for them in the database
        foreach ($users as $user)
        {
            User::create($user);
        }

        Model::reguard();
    }
}
