<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

use App\Friend;

class FriendsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        DB::table('friends')->delete();

        $friends = array(
            ['user_id' => 1, 'friend_user_id' => 2, 'status' => 'accepted'],
            ['user_id' => 3, 'friend_user_id' => 1, 'status' => 'pending'],
        );

        // Loop through each user above and create the record for them in the database
        foreach ($friends as $friend)
        {
            Friend::create($friend);
        }

        Model::reguard();
    }
}
