<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Match extends Model
{
    protected $fillable = ['start_date', 'user_id', 'group_id'];

    public function user(){
        return $this->belongsTo(User::class);
    }
    public function group(){
        return $this->belongsTo(Group::class);
    }
    public function scores(){
        return $this->hasMany(Score::class, 'match_id');
    }

    public function getWinner(){
        $scores = Score::where('match_id', '=', $this->id)->get();
        $scoresSorted = [];
        foreach($scores as $score){
            if(!isset($scoresSorted[$score->group_user_id])){
                $scoresSorted[$score->group_user_id] = array('group_user' => $score->group_user, 'points' => $score->points);
            } else {
                $scoresSorted[$score->group_user_id]['points'] += $score->points;
            }
        }

        $winner = null;
        $no_winner = false;
        foreach($scoresSorted as $score){
            if(!$winner){
                $winner = $score;
            } else{
                if($score['points'] > $winner['points']){
                    $winner = $score;
                    $no_winner = false;
                } else if($score['points'] == $winner['points']){
                    $no_winner = true;
                }
            }
        }
        if($no_winner){
            $winner = null;
        }
        if($winner){
            $winner = $winner['group_user']->user;
        }

        return $winner;
    }
    /**
     * Get nr of scores for a match
     *
     * @return int
     */
    public function nrOfScores(){
        return $this->scores()->count();
    }
}
