<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Friend extends Model
{
    const STATUS_PENDING = 'pending';
    const STATUS_ACCEPTED = 'accepted';
    const STATUS_DECLINED = 'declined';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'friend_user_id'];

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function friend_user(){
        return $this->belongsTo(User::class);
    }


    /**
     * Check if a user has a friend relationship already
     *
     * @param \App\User $user
     * @param \App\User $requested_user
     *
     * @return bool
     */
    public static function isFriend(User $user, User $requested_user){
        if(Friend::where('user_id', '=', $user->id)->where('friend_user_id', '=', $requested_user->id)->first()){
            return true;
        }
        if(Friend::where('user_id', '=', $requested_user->id)->where('friend_user_id', '=', $user->id)->first()){
            return true;
        }

        return false;
    }

    public static function getOwnFriends(User $user){
        return Friend::where('user_id', '=', $user->id)->orWhere('friend_user_id', '=', $user->id);;

    }
}
