<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    protected $fillable = ['user_id', 'name', 'game'];

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function group_users(){
        return $this->hasMany(GroupUser::class, 'group_id');
    }

    public function matches(){
        return $this->hasMany(Match::class, 'group_id');
    }

    /**
     * Get number of users for this group
     *
     * @return int
     */
    public function nrOfUsers(){
        return $this->group_users()->count();
    }

    /**
     * Get number of matches for this group
     *
     * @return int
     */
    public function nrOfMatches(){
        return $this->matches()->count();
    }

    /**
     * Get a list of groups the User has access to
     *
     * @param User $user
     *
     * @return array
     */
    public static function getGroups(User $user){
        $groups = [];

        $groups_users = GroupUser::where('user_id', '=', $user->id)->get();
        foreach($groups_users as $groups_user){
            $groups[] = $groups_user->group;
        }

        return $groups;

    }
}
