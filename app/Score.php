<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use PhpParser\Node\Stmt\GroupUse;

class Score extends Model
{
    const STATUS_PENDING = 'pending';
    const STATUS_ACCEPTED = 'accepted';
    const STATUS_DECLINED = 'declined';

    protected $fillable = ['group_user_id', 'match_id', 'status', 'points'];


    public function user(){
        return $this->belongsTo(User::class);
    }

    public function group_user(){
        return $this->belongsTo(GroupUser::class);
    }
}
