<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {
    Route::get('/', function () {
        return view('login');
    });
    Route::get('/login', function () {
        return redirect('/');
    });
    Route::get('/register', 'RegisterController@index');

    Route::group(['prefix' => 'api'], function(){
       Route::auth();
        Route::group(['middleware' => 'auth'], function(){
            Route::resource('/friends', 'ApiFriendsController');
        });
    });
});

// Route group for authenticated users
Route::group(['middleware' => ['web', 'auth']], function(){
    Route::get('/dashboard', 'DashboardController@index');
    Route::get('/friends', 'FriendsController@index');

    Route::get('/friends', 'FriendsController@index');
    Route::post('/friends', 'FriendsController@addFriend');

    Route::get('/games', 'GamesController@index');

    Route::get('/groups', 'GroupsController@index');
    Route::get('/groups/{id}', 'GroupsController@show');
    Route::post('/groups', 'GroupsController@createGroup');
    Route::post('/groups/{id}/add-user', 'GroupsController@addUserToGroup');
    Route::post('/groups/{id}/create-match', 'GroupsController@createMatch');
    Route::get('/groups/{id}/matches/{match_id}', 'MatchController@index');
    Route::post('/groups/{id}/matches/{match_id}/add-score', 'MatchController@addScore');

    Route::get('/scores', 'ScoreController@index');
});

