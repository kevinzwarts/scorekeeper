<?php

namespace App\Http\Controllers;

use App\Friend;
use App\Group;
use App\GroupUser;
use App\Match;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class GroupsController extends BaseController
{

    /**
     * Show list of all groups for current user
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(){
        $user = Auth::user();
        $groups = Group::getGroups($user);

        return view('groups', $this->getViewData(['groups' => $groups]));
    }

    /**
     * Show specific group, searched by id
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Request $request, $id){
        $validator = Validator::make($request->all(), []);

        $group = Group::find($id);

        if(!$group){
            $validator->errors()->add('id', 'No group found by given id');
            return Redirect::back()->withErrors($validator);
        }
        $group_users = $group->group_users;

        $matches = Match::where('group_id', '=', $group->id)->orderBy('start_date', 'desc')->get();


        return view('group-info', $this->getViewData(['group' => $group, 'group_users' => $group_users, 'matches' => $matches]));

    }

    /**
     * Create new group
     *
     * @param Request $request
     * @return mixed
     */
    public function createGroup(Request $request){
        $user = Auth::user();
        $group = Group::create([
            'user_id' => $user->id,
            'name' => $request->get('name'),
            'game' => $request->get('game')
        ]);

        GroupUser::create([
            'user_id' => $user->id,
            'group_id' => $group->id,
            'status' => GroupUser::STATUS_ACCEPTED
        ]);
        return Redirect::back()->with('status', 'Groups create successfully');
    }

    /**
     * Add a friend user to an existing group
     *
     * @param Request $request
     * @param $id
     * @return mixed
     */
    public function addUserToGroup(Request $request, $id){
        $validator = Validator::make($request->all(), [
            'username' => 'required'
        ]);

        $user = Auth::user();
        $username = $request->get('username');

        if(!$group = Group::find($id)){
            $validator->errors()->add('id', 'No group found by given id');
            return Redirect::back()->withErrors($validator);
        }

        // Find user by username
        $requested_friend = User::where('username', '=', $username)->orWhere('email', '=', $username)->first();
        if(!$requested_friend) {
            $validator->errors()->add('username', 'The user has already been added as a friend. The friend request might be pending.');
            return Redirect::back()->withErrors($validator);
        }
        // Check if user is a friend
        if(!Friend::isFriend($user, $requested_friend)){
            $validator->errors()->add('username', 'The user has already been added as a friend. The friend request might be pending.');
            return Redirect::back()->withErrors($validator);
        }

        GroupUser::create([
            'user_id' => $requested_friend->id,
            'group_id' => $group->id,
            'status' => GroupUser::STATUS_ACCEPTED
        ]);

        return Redirect::back()->with('status', 'User successfully added to group');
    }

    /**
     * Create a new match to start score tracker for
     *
     * @param Request $request
     * @param $id
     *
     * @return mixed
     */
    public function createMatch(Request $request, $id){
        $validator = Validator::make($request->all(), ['start_date' => 'required|date']);
        $user = Auth::user();

        if(!$group = Group::find($id)){
            $validator->errors()->add('id', 'No group found by given id');
            return Redirect::back()->withErrors($validator);
        }

        Match::create([
            'start_date' => $request->get('start_date'),
            'group_id' => $group->id,
            'user_id' => $user->id
        ]);

        return Redirect::back()->with('status', 'Match created succesfully');
    }
}
