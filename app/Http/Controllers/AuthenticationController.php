<?php

namespace App\Http\Controllers;

use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use GuzzleHttp\Client;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Input;

use App\Facades\ScoreKeeperApi;


class AuthenticationController extends Controller
{

    public function index(){
        return view('login');
    }

    public function logout(Request $request){
        session(['token' => null]);

        return redirect('/');
    }
}
