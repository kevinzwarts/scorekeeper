<?php

namespace App\Http\Controllers;

use App\Group;
use App\Match;
use App\Score;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class MatchController extends BaseController
{
    public function index(Request $request, $id, $match_id){
        $validator = Validator::make($request->all(), []);
        $group = Group::find($id);
        $match = Match::find($match_id);

        if(!$group){
            $validator->errors()->add('id', 'No group found by given id');
            return Redirect::back()->withErrors($validator);
        }

        if(!$match){
            $validator->errors()->add('id', 'No match found by given id');
            return Redirect::back()->withErrors($validator);
        }

        $group_users = $group->group_users;

        $scores = Score::where('match_id', '=', $match->id)->get();
        $scoresSorted = [];
        foreach($scores as $score){
            if(!isset($scoresSorted[$score->group_user_id])){
                $scoresSorted[$score->group_user_id] = array('group_user' => $score->group_user, 'points' => $score->points);
            } else {
                $scoresSorted[$score->group_user_id]['points'] += $score->points;
            }
        }

        return view('matches', $this->getViewData(['match' => $match, 'group' => $group, 'scores' => $scoresSorted, 'group_users' => $group_users]));
    }

    /**
     * Add a new score for a match
     *
     * @param Request $request
     * @param $id
     * @param $match_id
     * @return mixed
     */
    public function addScore(Request $request, $id, $match_id){
        $validator = Validator::make($request->all(), []);
        $group = Group::find($id);
        $match = Match::find($match_id);


        if(!$group){
            $validator->errors()->add('id', 'No group found by given id');
            return Redirect::back()->withErrors($validator);
        }

        if(!$match){
            $validator->errors()->add('id', 'No match found by given id');
            return Redirect::back()->withErrors($validator);
        }

        $inputs = $request->all();
        foreach($inputs['score'] as $group_user_id => $points){
            Score::create([
                'status' => Score::STATUS_ACCEPTED,
                'points' => $points,
                'group_user_id' => $group_user_id,
                'match_id' => $match->id
            ]);
        }

        return Redirect::back()->with('status', 'Score added succesfully');
    }
}
