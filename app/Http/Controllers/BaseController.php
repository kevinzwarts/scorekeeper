<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class BaseController extends Controller
{
    private $view_data = [];

    public function __construct(){
        $this->view_data['user'] = Auth::user();
    }

    public function getViewData(array $data = []){
        return array_merge($this->view_data, $data);
    }
}
