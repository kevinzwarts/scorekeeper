<?php

namespace App\Http\Controllers;

use App\Friend;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class FriendsController extends BaseController{

    public function index(){
        $user = Auth::user();
        $friend_list = [];
        $data = [];

//        $friends = Friend::where('status', '=', 'accepted')->where('user_id', '=', $user->id)->orWhere('friend_user_id', '=', $user->id)->get();
        $friends = Friend::where('user_id', '=', $user->id)->orWhere('friend_user_id', '=', $user->id)->get();

        foreach($friends as $friend) {
            if ($friend->user->id != $user->id) {
                $friend_list[] = $friend->user;
            } else if ($friend->friend_user->id != $user->id) {
                $friend_list[] = $friend->friend_user;
            }
        }
        $data['friends'] = $friend_list;

        return view('friends', $this->getViewData($data));
    }


    /**
     * Add friend to friend list
     *
     * @param Request $request
     */
    public function addFriend(Request $request){
        $validator = Validator::make($request->all(), [
            'username' => 'required'
        ]);

        $user = Auth::user();
        $username = $request->get('username');

        $requested_friend = User::where('username', '=', $username)->orWhere('email', '=', $username)->first();
        if(!$requested_friend) {
            $validator->errors()->add('username', 'No user found by given username/email');
            return Redirect::back()->withErrors($validator);
        }

        if(Friend::isFriend($user, $requested_friend)){
            $validator->errors()->add('username', 'The user has already been added as a friend. The friend request might be pending.');
            return Redirect::back()->withErrors($validator);
        }

        // Check if friend is already in list

        Friend::create([
            'user_id' => $user->id,
            'friend_user_id' => $requested_friend->id,
            'status' => Friend::STATUS_ACCEPTED
        ]);

        return Redirect::back()->with('status', 'Friend successfully added');
    }
}
