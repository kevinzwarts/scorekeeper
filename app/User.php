<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

use App\Friend;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'username', 'password',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function friends(){
        $friends = $this->invited_friends();
        foreach($this->invited_by_friends() as $invitedByFriend){
            $friends->add($invitedByFriend);
        }
        return $friends;
    }

    /**
     * Friends that i invited
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function invited_friends(){
        return $this->hasMany(Friend::class, 'user_id');
    }

    /**
     * Friends that i invited
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function invited_by_friends(){
        return $this->hasMany(Friend::class, 'friend_user_id');
    }
}
