@extends('layout.master')

@section('content')

    <h1>Group {{ $group->name }}</h1>
    <p>
        Add friends to your group to have matches against eachother. Make sure to add friends before starting a match. When a match has been added to a group it is no longer possible to add friends to the group.
    </p>
    <hr class="thin" />
    @if($group->nrOfMatches() === 0)
    <button class="button primary" onclick="showMetroDialog('#addUserDialog')"><span class="mif-user-plus"></span> Add user to group</button>
    @endif
    <button class="button primary" onclick="showMetroDialog('#createMatchDialog')"><span class="mif-plus"></span> Create new match</button>
    <hr class="thin" />
    @parent

    <h2>Matches</h2>
    @if(count($matches) > 0)
        <p>Matches that have been added to the current group.</p>
        <button class="button primary" onclick="showHideToggle('#group-matches')">Show/hide</button>
        <p></p>
        <table id="group-matches" class="table striped border cell-hovered hovered">
            <thead>
            <tr>
                <th class="sortable-column sort-asc">start date</th>
                <th class="sortable-column sort-asc">winner</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            @foreach ( $matches as $match)
                <tr>
                    <td>{{ $match->start_date }}</td>
                    <td>{{ ($match->getWinner() ? $match->getWinner()->username : 'Undecided') }}</td>
                    <td><a href="/groups/{{ $group->id }}/matches/{{ $match->id }}">Open</a></td>
                </tr>
            @endforeach
            </tbody>
        </table>
    @else
        <p>This group has no matches yet</p>
    @endif

    <h2>Added friends</h2>
    @if(count($group_users) > 0)
        <p>Friends that have been added to the current group to play matches against.</p>
        <button class="button primary" onclick="showHideToggle('#group-users')">Show/hide</button>
        <p></p>
        <table id="group-users" class="table striped border cell-hovered hovered">
            <thead>
            <tr>
                <th class="sortable-column sort-asc">username</th>
                <th class="sortable-column sort-asc">name</th>
                <th class="sortable-column sort-asc">email</th>
            </tr>
            </thead>
            <tbody>
            @foreach ( $group_users as $group_user)
                <tr>
                    <td>{{ $group_user->user->username }}</td>
                    <td>{{ $group_user->user->name }}</td>
                    <td>{{ $group_user->user->email }}</td>

                </tr>
            @endforeach
            </tbody>
        </table>
    @else
        <p>This group has no members yet</p>
    @endif


    <div data-role="dialog" id="addUserDialog" data-close-button="true" class="padding20">
        <h1>Add user to group "{{ $group->name }}"</h1>
        <p>Add a user to your group to keep score with.</p>
        <div class="margin20 no-margin-left no-margin-right no-margin-bottom">
            {{ Form::open(['url' => url('/groups/' . $group->id . '/add-user'), 'method' => 'post' ]) }}
            <div class="input-control text full-size" data-role="input">
                <label for="name">Username/email:</label>
                {{ Form::text('username') }}
                <button class="button helper-button clear" tabindex="-1" type="button"><span class="mif-cross"></span></button>
            </div>
            <br />
            <br />
            <div class="form-actions">
                <button type="submit" class="button primary"><span class="mif-user-plus"></span> Add user to group</button>
                <a onclick="hideMetroDialog('#addUserDialog')" class="button link">Cancel</a>
            </div>
            {{ Form::close() }}
        </div>
        <span class="dialog-close-button"></span>
    </div>

    <div data-role="dialog" id="createMatchDialog" data-close-button="true" class="padding20">
        <h1>Create new match for group "{{ $group->name }}"</h1>
        <p>By creating a match it is possible to keep scores with your friends</p>
        <div class="margin20 no-margin-left no-margin-right no-margin-bottom">
            {{ Form::open(['url' => url('/groups/' . $group->id . '/create-match'), 'method' => 'post' ]) }}
            <div class="input-control text full-size" data-role="input">
                <label for="start_date">Date of the match:</label>
                {{ Form::date('start_date') }}
                <button class="button helper-button clear" tabindex="-1" type="button"><span class="mif-cross"></span></button>
            </div>
            <br />
            <br />
            <div class="form-actions">
                <button type="submit" class="button primary"><span class="mif-user-plus"></span> Add user to group</button>
                <a onclick="hideMetroDialog('#createMatchDialog')" class="button link">Cancel</a>
            </div>
            {{ Form::close() }}
        </div>
        <span class="dialog-close-button"></span>
    </div>
@stop