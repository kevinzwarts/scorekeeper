@extends('layout.login')

@section('content')
    <div class="login-form padding20 block-shadow bg-white ">
        {{ Form::open(['url' => '/api/register', 'action' => 'post']) }}
            <h1 class="text-light">Register</h1>
            <hr class="thin">
            <br>
            <div class="grid">
                @parent
                <div class="input-control password full-size" data-role="input">
                    <label for="password">Name:</label>
                    {{ Form::text('name') }}
                    <button class="button helper-button clear" tabindex="-1" type="button"><span class="mif-cross"></span></button>
                </div>
                <br />
                <br />
                <div class="input-control password full-size" data-role="input">
                    <label for="password">Username:</label>
                    {{ Form::text('username') }}
                    <button class="button helper-button clear" tabindex="-1" type="button"><span class="mif-cross"></span></button>
                </div>
                <br />
                <br />
                <div class="input-control text full-size" data-role="input">
                    <label for="email">Email:</label>
                    {{ Form::text('email') }}
                    <button class="button helper-button clear" tabindex="-1" type="button"><span class="mif-cross"></span></button>
                </div>
                <br />
                <br />
                <div class="input-control password full-size" data-role="input">
                    <label for="password">Password:</label>
                    {{ Form::password('password') }}
                    <button class="button helper-button reveal" tabindex="-1" type="button"><span class="mif-looks"></span></button>
                </div>
                <br />
                <br />
                <div class="input-control password full-size" data-role="input">
                    <label for="password">Password confirmation:</label>
                    {{ Form::password('password_confirmation') }}
                    <button class="button helper-button reveal" tabindex="-1" type="button"><span class="mif-looks"></span></button>
                </div>
                <br />
                <br />
                <div class="form-actions">
                    <button type="submit" class="button primary">Register</button>
                    <a href="/" class="button link">Cancel</a>
                </div>
            </div>
        {{ Form::close() }}
    </div>
@stop
