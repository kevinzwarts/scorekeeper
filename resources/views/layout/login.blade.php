<html>
<head>
    <script type="text/javascript" src="/assets/js/jquery-2.1.3.min.js"></script>
    <script type="text/javascript" src="/assets/metro/build/js/metro.min.js"></script>
    <link rel="stylesheet" href="/assets/metro/build/css/metro-icons.min.css">
    <link rel="stylesheet" href="/assets/metro/build/css/metro.min.css"/>
    <link rel="stylesheet" href="/assets/metro/build/css/metro-schemes.min.css"/>
    <style>
        .login-form{
            position:fixed;
            width: 25rem;
            min-height: 18.75rem;
            position: fixed;
            top: 50%;
            margin-top: -9.375rem;
            left: 50%;
            margin-left: -12.5rem;
        }
    </style>
</head>
<body class="bg-lighterGray">
@section('menu')
    <header class="app-bar">
        <div class="container">
            <a class="app-bar-element" href="/"><img src="/assets/img/logo.png" /></a>
        </div>
    </header>
@show
<div class="container responsive">
    @section('content')
            @if (count($errors) > 0)
                <div class="alert alert-danger fg-red">
                @foreach ($errors->all() as $error)
                    <div><span class="input-state-error mif-warning fg-red"></span> {{ $error }}</div>
                @endforeach
                    <br />
                    <br />
                </div>
            @endif
        @show

    @show
</div>
</div>

</body>
</html>