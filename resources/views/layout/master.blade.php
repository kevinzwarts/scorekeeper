<html>
<head>
    <script type="text/javascript" src="/assets/js/jquery-2.1.3.min.js"></script>
    <script type="text/javascript" src="/assets/metro/build/js/metro.min.js"></script>
    <link rel="stylesheet" href="/assets/metro/build/css/metro-icons.min.css">
    <link rel="stylesheet" href="/assets/metro/build/css/metro.min.css"/>
    <link rel="stylesheet" href="/assets/metro/build/css/metro-schemes.min.css"/>

    <script>
        function showHideToggle(id){
            console.log(id);
            $(id).toggle();
        }
    </script>

</head>
<body class="bg-lighterGray">
@section('menu')
    <header class="app-bar">
        <div class="container">
            <a class="app-bar-element" href="/"><img src="/assets/img/logo.png" /></a>
            <ul class="app-bar-menu place-right">
                <li><span class="padding10 no-padding-bottom no-padding-top">{{ $user->email }}</span></li>
                <li><span class="app-bar-divider"></span></li>
                <li><a href="/api/logout">Logout</a></li>
            </ul>
        </div>
    </header>
@show
<div class="container responsive">
    <div class="container responsive bg-white block-shadow">
        <div class="grid no-margin">
            <div class="row cells4">
                @section('sidebar')
                    <div class="cell">
                        <ul class="sidebar darcula">
                            <li>
                                <a href="/dashboard">
                                    <span class="icon mif-home"></span>
                                    <span class="title">Dashboard</span>
                                    <span class="icon"></span>
                                </a>
                            </li>
                            <li>
                                <a href="/friends">
                                    <span class="icon mif-users"></span>
                                    <span class="title">Friends</span>
                                    <span class="icon"></span>
                                </a>
                            </li>
                            <li>
                                <a href="/games">
                                    <span class="icon mif-gamepad"></span>
                                    <span class="title">Games</span>
                                    <span class="icon"></span>
                                </a>
                            </li>
                            <li>
                                <a href="/groups">
                                    <span class="icon mif-organization"></span>
                                    <span class="title">Groups</span>
                                    <span class="icon"></span>
                                </a>
                            </li>
                            <li>
                                <a href="/scores">
                                    <span class="icon mif-target"></span>
                                    <span class="title">Scores</span>
                                    <span class="icon"></span>
                                </a>
                            </li>
                        </ul>
                    </div>
                @show
                <div class="cell colspan3">
                @section('content')
                    @section('messages')
                        @if (count($errors) > 0)
                            <div class="alert alert-danger fg-red margin20 no-margin-left no-margin-right">
                                @foreach ($errors->all() as $error)
                                    <div><span class="input-state-error mif-warning fg-red"></span> {{ $error }}</div>
                                @endforeach
                            </div>
                        @endif
                        @if(session('status'))
                            <div class="alert alert-success fg-green margin20 no-margin-left no-margin-right">
                                <div><span class="input-state-error mif-checkmark fg-green"></span> {{ session('status') }}</div>
                            </div>
                        @endif
                    @show
                @show
                </div>
            </div>
        </div>
    </div>
    </div>
</div>

</body>
</html>