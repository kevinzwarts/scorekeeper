@extends('layout.master')

@section('content')
    <h1>Friends</h1>
    <hr class="thin" />

    @parent
    <button class="button primary" onclick="showMetroDialog('#addFriendDialog')"><span class="mif-user-plus"></span> Add friend</button>
    <hr class="thin" />
    @if (count($friends) > 0)
    <table class="table striped border cell-hovered hovered">
        <thead>
        <tr>
            <th class="sortable-column">username</th>
            <th class="sortable-column sort-asc">e-mail</th>
            <th class="sortable-column sort-desc">name</th>
        </tr>
        </thead>
        <tbody>
        @foreach ( $friends as $friend)
        <tr>
            <td>{{ $friend->username }}</td>
            <td>{{ $friend->email }}</td>
            <td>{{ $friend->name }}</td>
        </tr>
        @endforeach
        </tbody>
    </table>
    @else
        <p>You have no available friends yet</p>
    @endif
    <div data-role="dialog" id="addFriendDialog" data-close-button="true" class="padding20">
        <h1>Add a friend</h1>
        <p>Find a user by username or email address and send him a friend request.</p>
        <div class="margin20 no-margin-left no-margin-right no-margin-bottom">
            {{ Form::open(['url' => url('friends'), 'method' => 'post' ]) }}
                <div class="input-control text full-size" data-role="input">
                    <label for="username">Username/email:</label>
                    {{ Form::text('username') }}
                    <button class="button helper-button clear" tabindex="-1" type="button"><span class="mif-cross"></span></button>
                </div>

                <div class="form-actions">
                    <button type="submit" class="button primary"><span class="mif-user-plus"></span> Add friend</button>
                    <a onclick="hideMetroDialog('#addFriendDialog')" class="button link">Cancel</a>
                </div>
            {{ Form::close() }}
        </div>
        <span class="dialog-close-button"></span>
    </div>
@stop