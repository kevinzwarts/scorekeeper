@extends('layout.master')

@section('content')

    <h1>Match start {{ $match->start_date}}</h1>
    <p>Matches {{ $match->start_date }} from group {{ $group->name }} with game {{ $group->game }}</p>
    <hr class="thin" />
    <button class="button primary" onclick="showMetroDialog('#addScoreDialog')"><span class="mif-plus"></span> Add new score</button>
    <hr class="thin" />
    @parent
    <h2>Scores</h2>
    @if(count($scores) > 0)
        <p>Matches that have been added to the current group.</p>
        <button class="button primary" onclick="showHideToggle('#group-matches')">Show/hide</button>
        <p></p>
        <table id="group-matches" class="table striped border cell-hovered hovered">
            <thead>
            <tr>
                <th class="sortable-column sort-asc">start date</th>
                <th class="sortable-column sort-asc">scores</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            @foreach ( $scores as $score)
                <tr>
                    <td>{{ $score['group_user']->user->username }}</td>
                    <td>{{ $score['points'] }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    @else
        <p>This match has no scores yet</p>
    @endif
    <div data-role="dialog" id="addScoreDialog" data-close-button="true" class="padding20">
        <h1>Add new score</h1>
        <p>Insert new score for current match.</p>
        <div class="margin20 no-margin-left no-margin-right no-margin-bottom">
            {{ Form::open(['url' => url('/groups/' . $group->id .'/matches/' . $match->id . '/add-score'), 'method' => 'post' ]) }}
            @foreach($group_users as $group_user)
                <div class="input-control text full-size" data-role="input">
                    <label for="name">{{ $group_user->user->username }}:</label>
                    {{ Form::text('score[' . $group_user->id . ']') }}
                    <button class="button helper-button clear" tabindex="-1" type="button"><span class="mif-cross"></span></button>
                </div>
                <br />
                <br />
            @endforeach
            <div class="form-actions">
                <button type="submit" class="button primary"><span class="mif-plus"></span> Add score</button>
                <a onclick="hideMetroDialog('#addScoreDialog')" class="button link">Cancel</a>
            </div>
            {{ Form::close() }}
        </div>
        <span class="dialog-close-button"></span>
    </div>
@stop