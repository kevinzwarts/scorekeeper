@extends('layout.login')

@section('content')
    <div class="login-form padding20 block-shadow bg-white ">
        {{ Form::open(['url' => url('api/login'), 'method' => 'post' ]) }}
            <h1 class="text-light">Login</h1>
            <hr class="thin">
            <br>
            <div class="grid">
                @parent
                <div class="input-control text full-size" data-role="input">
                    <label for="email">Email:</label>
                    {{ Form::text('email') }}
                    <button class="button helper-button clear" tabindex="-1" type="button"><span class="mif-cross"></span></button>
                </div>
                <br>
                <br>
                <div class="input-control password full-size" data-role="input">
                    <label for="password">Password:</label>
                    {{ Form::password('password') }}
                    <button class="button helper-button reveal" tabindex="-1" type="button"><span class="mif-looks"></span></button>
                </div>
                <br>
                <br>
                {{--<div class="full-size">--}}
                    {{--<label class="input-control checkbox">--}}
                        {{--<input type="checkbox" name="remember" checked>--}}
                        {{--<span class="check"></span>--}}
                        {{--<span class="caption">Remember login credentials</span>--}}
                    {{--</label>--}}
                {{--</div>--}}
                <div class="form-actions">
                    <button type="submit" class="button primary">Login</button>
                    <button type="button" class="button link">Cancel</button>
                    <a href="/register" class="button link">No account? Register now!</a>
                </div>
            </div>
        {{ Form::close() }}
    </div>
@stop
