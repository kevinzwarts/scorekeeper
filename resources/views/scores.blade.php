@extends('layout.master')

@section('content')

    <h1>Scores</h1>
    <hr class="thin" />
    <button class="button primary" onclick="showMetroDialog('#addUserDialog')"><span class="mif-plus"></span> Create new match</button>
    <hr class="thin" />
    @parent
@stop