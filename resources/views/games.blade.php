@extends('layout.master')

@section('content')

    <h1>Games</h1>
    <hr class="thin" />
    @parent
    <p>This section will be available later to add games you play to your account. With this functionality you can challange friends for a specific game you both own</p>
@stop