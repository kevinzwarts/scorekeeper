@extends('layout.master')

@section('content')

    <h1>Groups</h1>
    <hr class="thin" />
    <button class="button primary" onclick="showMetroDialog('#createGroupDialog')"><span class="mif-user-plus"></span> Create group</button>
    <hr class="thin" />
    @parent

    @if(count($groups) > 0)
    <table id="friends-list" class="table striped border cell-hovered hovered">
        <thead>
        <tr>
            <th class="sortable-column">group name</th>
            <th class="sortable-column sort-asc">game</th>
            <th class="sortable-column sort-asc">group owner</th>
            <th class="sortable-column sort-asc">users</th>
            <th class="sortable-coiumn sort-asc">matches</th>
            <th class="sortable-column"></th>
        </tr>
        </thead>
        <tbody>
        @foreach ( $groups as $group)
            <tr>
                <td>{{ $group->name }}</td>
                <td>{{ $group->game }}</td>
                <td>{{ $group->user->name }}</td>
                <td>{{ $group->nrOfUsers() }}</td>
                <td>{{ $group->nrOFMatches() }}</td>
                <td><a href="/groups/{{ $group->id }}">Open</a></td>
            </tr>
        @endforeach
        </tbody>
    </table>
    @else
    <p>You are not a member of any group yet</p>
    @endif


    <div data-role="dialog" id="createGroupDialog" data-close-button="true" class="padding20">
        <h1>Create group</h1>
        <p>Create a new group to keep scores with.</p>
        <div class="margin20 no-margin-left no-margin-right no-margin-bottom">
            {{ Form::open(['url' => url('groups'), 'method' => 'post' ]) }}
            <div class="input-control text full-size" data-role="input">
                <label for="name">Groups name:</label>
                {{ Form::text('name') }}
                <button class="button helper-button clear" tabindex="-1" type="button"><span class="mif-cross"></span></button>
            </div>
            <br />
            <br />
            <div class="input-control text full-size" data-role="input">
                <label for="username">Game:</label>
                {{ Form::text('game') }}
                <button class="button helper-button clear" tabindex="-1" type="button"><span class="mif-cross"></span></button>
            </div>
            <br />
            <br />
            <div class="form-actions">
                <button type="submit" class="button primary"><span class="mif-user-plus"></span> Create group</button>
                <a onclick="hideMetroDialog('#createGroupDialog')" class="button link">Cancel</a>
            </div>
            {{ Form::close() }}
        </div>
        <span class="dialog-close-button"></span>
    </div>
@stop